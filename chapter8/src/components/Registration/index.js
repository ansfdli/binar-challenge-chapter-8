import { useState, useRef, useEffect } from 'react';
import { Grid, TextField, Typography } from '@mui/material';
import './index.css'
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import StickyHeadTable from './table';

const Registration =() => {
  const theme = createTheme();
  const [confirmType, setConfirmType] = useState('password');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPasssword] = useState('');
  const [submittedData, setSubmittedData] = useState({
      username: '',
      email: '',
      password: '',
      confirm: '',
  });
  const formWrapperElem = useRef(null);

  useEffect(() => {
    console.log('functional componentDidMount')

    return () => {
      console.log('functional componentWillUnmount')
    }
  }, [])
  
  const handleSubmit = () => {
    setSubmittedData({
        username: username,
        email: email,
        password: password,
        confirm: confirmPassword,
    })
}
  const handleUsername = (data) => {
    setUsername(data.target.value)
}

const handleEmail = (data) => {
    setEmail(data.target.value)
}

const handlePassword = (data) => {
    setPassword(data.target.value)
}

const handleConfirmPassword = (data) => {
    setConfirmPasssword(data.target.value)
}
  
  return <div id="registration-wrapper" ref={formWrapperElem}>

  <ThemeProvider theme={theme}>
    <Container component="main" maxWidth="xs">
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
          >
        <Avatar sx={{ m: 1, bgcolor: 'warning.main' }}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component="h1" variant="h5" fontWeight= 'bold'>
          CREATE ACCOUNT
        </Typography>

        <Box component="form" sx={{ mt: 3 }}>
          <Grid container spacing={2}>

            <Grid item xs={12}>
              <TextField 
                required fullWidth 
                onChange={handleUsername}
                value={username} 
                id="yourName" 
                label="Your Name" 
                variant="outlined" />
              </Grid>

            <Grid item xs={12}>
              <TextField 
              required fullWidth 
              onChange={handleEmail}
              value={email} 
              id="email" 
              label="Your Email" 
              type="email"  
              variant="outlined" />
              </Grid>

            <Grid Grid item xs={12}>
              <TextField 
              required fullWidth 
              onChange={handlePassword}
              value={password} 
              id="password" 
              label="Password" 
              type={confirmType} 
              variant="outlined" />
              </Grid>

            <Grid item xs={12}>
              <TextField 
              required fullWidth 
              onChange={handleConfirmPassword}
              value={confirmPassword} 
              id="repeatPassword" 
              label="Repeat your password" 
              type={confirmType} 
              variant="outlined" />
              </Grid>

            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox
                  checked={confirmType === "text"}
                  onChange={() => {
                    if (confirmType === "text") {
                      setConfirmType("password")
                    } else {
                      setConfirmType("text")
                    }
                  }}
                  color="warning"
                /> }
                label="Show Password"
              />
              </Grid>

            <Button
              onClick={handleSubmit}
              type="submit"
              color="warning"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, ml:2 }}
              >
              SUBMIT
            </Button>

            <Grid item xs={6}>
                <div>{`Username: ${submittedData.username}`}</div>
                <div>{`Email: ${submittedData.email}`}</div>
                <div>{`Password: ${submittedData.password}`}</div>
                <div>{`Confirm: ${submittedData.confirm}`}</div>
            </Grid>

            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
    <StickyHeadTable rows={submittedData} />
  </div>
}

  export default Registration;
