import Registration from '../components/Registration';
import SearchPlayer from '../components/SearchPlayer';

const routes = [
    {
        path: '/',
        component: <Registration />,
        exact: true,
    },
    {
        path: '/search',
        component: <SearchPlayer />,
        exact: false,
        },

]

export default routes;